//WOW air scrapper One-Way

var cheerio = require('cheerio'),
_s = require('underscore.string'),
fs = require('fs'),
moment = require('moment');
require('twix');

//Ejemplo
var file = './wow/wow.html';

var getTrip = function(data){

	$ = cheerio.load(data);

	var itinerary = {},
		returnTrip = [],
		trip = {};

		//Informacion sobre a hora de salida
		trip.departureInfoTime = moment($('.calendar-basket>div.outbound>div.row.clear>div.right-column', data)
			.text(), 'DD MMM YYYY')
		.format('ddd, MMM DD,YYYY');

		//Hora de Salida
		trip.departureTime = moment($('.full-column.leg>span', data).text(), 'HH:mm').format('h:mm A');

		//Hora de salida y llegada de la primera parte del viaje
		var departureTime = moment($('.full-column.leg>span', data).text(), 'HH:mm A').format(),
		 firstLegArrivalTime = moment($('.full-column.leg>span:nth-child(2)', data).text(), 'HH:mm A').format();

		//Tiempo Formato Crudo
		trip.departureTimeRaw = moment(trip.departureInfoTime+' '+trip.departureTime,'ddd, MMM DD,YYYY hh:mm a').toISOString();

		//Ciudad de salida
		trip.departureCity = _s.clean($('.full-column>span', data).text().split(' ')[0]);

		//Aeropuerto de Salida
		trip.departureAirport = _s.clean($('.clear:first-child>span', data).text().split(' ')[2]);

		//Tiempo de llegada
		trip.arrivalTime = moment($('.clear>div:nth-child(4)>span:nth-child(2)' ,data).text(),'HH:mm').format('h:mm A');

		//Hora de salida y llegada de la segunda parte del viaje
		var arrivalTime = moment($('.clear>div:nth-child(4)>span:nth-child(2)' ,data).text(),'HH:mm A').format(),
		secondLegDepartureTime = moment($('.clear>div:nth-child(4)>span' ,data).text(),'HH:mm A').format();
		
		//Aeropuerto de Llegada
		trip.arrivalAirport =  _s.clean($('.clear:first-child>span', data).text().split(' ')[5]);

		//Ciudad de llegada
		trip.arrivalCity = _s.clean($('.clear:first-child>span', data).text().split(' ')[4]);

		//Numero de paradas que hara el viaje
		trip.stops = 1;

		//Se calcula el tiempo aproximado que duraran los dos tramos del viaje
		var firstLeg = parseInt(moment(firstLegArrivalTime).twix(departureTime).countInner("hours")),
		 secondLeg = parseInt(moment(secondLegDepartureTime).twix(arrivalTime).countInner("hours"));
		
		//Tiempo aproximado de duracion del viaje
		trip.duration = (firstLeg-1)+secondLeg+'h00m';

		//Eliminacion de basura para obtener solo numeros
		var durArr = trip.duration
		.replace('h',' ')
		.replace('m',' ')
		.split(' ');


		//Objeto parseado para separar minutos, horas y días.
		trip.durationObj={};
		trip.durationObj.days = parseInt(durArr[0]/24); //Dias
		trip.durationObj.hours = durArr[0]%24;			//Horas
		trip.durationObj.mins = parseInt(durArr[1]);    //Minutos

		//Viaje operado por
		trip.operatedBy =  "Air Via"; 

		//Nombre de la Aerolinea
		trip.airLine= "WOW Air";

		//Codigo de la aerolinea.  El codigo es 00 si son aerolineas multiples
		trip.airLineCode = "WW";

		returnTrip.push(trip);
		/*
		trip.mileage: '', //Número entero [opcional]. Incluir el número de millas si se cuenta con el dato
        trip.originTerminal: "", //[opcional] La terminal del aeropuerto donde saldrá
        trip.destinationTerminal: "", //[opcional] La terminal del aeropuerto donde llega
	    trip.meal: "" //[opcional] El tipo de comida que se sirve
*/
		//Insertando segmento de viaje trips[{},{},{}]
		itinerary.trips = returnTrip;
		//Costo total del viaje
		itinerary.totalCost = parseInt($('.right-column.total-price', data).text().replace(' ','').replace('$',''));
		itinerary.url = '';
		itinerary.provider = "WOW Air"; //Proveedor

		return [itinerary];

}

//Funcion para abrir el archivo a parsear.
fs.readFile(file, {encoding: 'utf-8'}, function (err, data) {
	if (err) throw err;

	var itineraries = getTrip(data);
	console.log(JSON.stringify(itineraries));
});